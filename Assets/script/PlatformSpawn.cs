﻿using UnityEngine;

public class PlatformSpawn : MonoBehaviour
{
    [SerializeField]
    GameObject[] _patternList;

    GameObject _loadedPattern;

	// Use this for initialization
	void Start ()
    {
        // Spawn a platform right away
        __spawnPlatform();
	}

    // Update is called once per frame
    void Update()
    {
        BoxCollider2D localCollider = GetComponent<BoxCollider2D>();
        BoxCollider2D patternCollider = _loadedPattern.GetComponent<BoxCollider2D>();

        // Check to see if the colliders are touching/on top of each other
        if (!patternCollider.bounds.Intersects(localCollider.bounds))
            __spawnPlatform();
    }

    void __spawnPlatform()
    {
        int randomIndex = UnityEngine.Random.Range(0, _patternList.Length);
        
        _loadedPattern = (GameObject)Instantiate(_patternList[randomIndex], transform.position, Quaternion.identity);
    }
}

﻿using UnityEngine;
using System;
using System.Collections;

public class GarbageCollector : MonoBehaviour
{
	public static event Action OnPlayerDeath;

    void OnTriggerEnter2D(Collider2D collider)
    {
    	if (collider.gameObject.layer == PhysicsLayers.PLAYER_LAYER)
    	{
    		if (OnPlayerDeath != null)
    		{
    			OnPlayerDeath ();
    		}
    	}
    	
		Destroy(collider.gameObject);
    }
}

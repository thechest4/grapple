﻿using UnityEngine;
using System;
using System.Collections;

public class GrappleProjectileController : MonoBehaviour 
{
	private Vector3 direction = Vector3.zero;
	[SerializeField]
	private float velocity = 6.0f;
	[SerializeField]
	private float maxDistance = 10.0f;
	[SerializeField]
	private Rigidbody2D body;
	
	public Transform jointConnectionPoint;
	
	private Transform owner;
	private bool isAttached = false;
	
	//TODO: Find some way to attribute ownership of the projectile
	public static event Action<Transform> OnGrappleProjectileCollision;
	
	public void Initialize (Vector3 argDirection, Transform argOwner)
	{
		direction = argDirection;
		body.velocity = direction * velocity;
		owner = argOwner;
	}
	
	void Update ()
	{
		float distance = Vector3.Distance (transform.position, owner.position);
		if (distance > maxDistance)
		{
			ReelIn ();
		}
	}
	
	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.collider.gameObject.layer == LayerMask.NameToLayer ("Ground"))
		{
			body.velocity = Vector2.zero;
			isAttached = true;
			
			if (col.collider.transform.parent != null)
			{
				//There's a bug in Unity where if a child of a non-uniformly scaled parent is rotated, it becomes skewed.  In order to combat this, we need all our grapple-targets to have uniformly scaled parents
				transform.parent = col.collider.transform.parent;
			}
			
			FireOnGrappleProjectileCollisionEvent ();
		}
	}
	
	private void FireOnGrappleProjectileCollisionEvent ()
	{
		if (OnGrappleProjectileCollision != null)
		{
			OnGrappleProjectileCollision (jointConnectionPoint);
		}
	}
	
	public bool ReelIn ()
	{
		if (!isAttached)
		{
			//TODO: implement a reel in function
			GameObject.Destroy (gameObject);
			return true;
		}
		else return false;
	}
}

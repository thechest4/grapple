﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifespanTimer : MonoBehaviour
{
	[SerializeField]
	private Text timerText;

	private bool isGameInProgress = false;
	private float timerStartedTimestamp = 0.0f;

    void Start()
    {
    	GameManager.OnGameStarted += HandleOnGameStarted;
    }

    void HandleOnGameStarted ()
    {
    	StartTimer ();
    }

	private void StartTimer ()
	{
		isGameInProgress = true;
		timerStartedTimestamp = Time.timeSinceLevelLoad;
	}
	
	public void StopTimer ()
	{
		isGameInProgress = false;
	}
	
	public string GetScore ()
	{
		return timerText.text;
	}

	void Update ()
	{
		if (isGameInProgress)
		{
			timerText.text = UpdateTimeDisplay ();
		}
	}

	private string UpdateTimeDisplay ()
	{
		int currentScoreInSeconds = (int) (Time.timeSinceLevelLoad - timerStartedTimestamp);
		int currentScoreInMinutes = currentScoreInSeconds / 60;
		string minutesDisplay = (currentScoreInMinutes < 10) ? "0" + currentScoreInMinutes : "" + currentScoreInMinutes;
		int remainderInSeconds = currentScoreInSeconds % 60;
		string secondsDisplay = (remainderInSeconds < 10) ? "0" + remainderInSeconds : "" + remainderInSeconds;

		string toReturn = minutesDisplay + ":" + secondsDisplay;
		return toReturn;
	}
	
	void OnDestroy()
	{
		GameManager.OnGameStarted -= HandleOnGameStarted;
	}
	
}


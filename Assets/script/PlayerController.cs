using UnityEngine;
using System;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	[SerializeField]
	private GrappleController grappleController;

	[SerializeField]
	private float jumpForce = 15.0f;
	[SerializeField]
	private float airJumpVelocity = 10.0f;
	[SerializeField]
	private int maxAirJumps = 1;
	[SerializeField]
	private float groundedMoveForce = 50.0f;
	[SerializeField]
	private float maxGroundVelocity = 7.5f;
	[SerializeField]
	private float aerialMoveForce = 10.0f;
	[SerializeField]
	private Vector2 wallJumpVelocity = Vector2.zero;
	
	[SerializeField]
	private Rigidbody2D body;
	[SerializeField]
	private Collider2D col;
	[SerializeField]
	private Animator animator;

	[SerializeField]
	private Transform raycastPositionTop;
	[SerializeField]
	private Transform raycastPositionMid;
	[SerializeField]
	private Transform raycastPositionBot;

	private bool isTouchingWallRight = false;
	private bool isTouchingWallLeft = false;
	private bool isGrounded = false;
	private int currAirJumps;

	const float rightFacingLocalScale = 2.0f;

	public event Action OnJump;
	
	void Start ()
	{
		currAirJumps = maxAirJumps;
		grappleController.OnGrappleAttached += HandleOnGrappleAttached;
	}
	
	void Update ()
	{
		UpdateGroundedStatus ();
		UpdateWallTouchStatus ();
		
		//TODO: Move all inputs into one class instead of having each script that needs input track it's own
//		if (Input.GetKeyDown (KeyCode.Space) && isGrounded) 
		if (Input.GetButtonDown ("Jump") && isGrounded) 
		{
			//TODO: Make jump cancelling a grapple feel better (probably needs a separate force entry than a normal jump
			body.AddForce (jumpForce * transform.up, ForceMode2D.Impulse);
			
			FireOnJumpEvent ();
		} 
		else if (Input.GetButtonDown ("Jump") && !isGrounded && (isTouchingWallLeft || isTouchingWallRight)) 
		{
			if (isTouchingWallLeft && isTouchingWallRight)
			{
				body.velocity = new Vector2 (body.velocity.x, wallJumpVelocity.y);
			}
			else if (isTouchingWallLeft)
			{
				body.velocity = new Vector2 (wallJumpVelocity.x, wallJumpVelocity.y);
				transform.localScale = new Vector3 (rightFacingLocalScale, transform.localScale.y);
			}
			else if (isTouchingWallRight)
			{
				body.velocity = new Vector2 (-wallJumpVelocity.x, wallJumpVelocity.y);
				transform.localScale = new Vector3 (-rightFacingLocalScale, transform.localScale.y);
			}
			
			RegainAirJumps ();
//			currAirJumps--;
			FireOnJumpEvent ();
		}	
		else if (Input.GetButtonDown ("Jump") && !isGrounded && currAirJumps > 0)
		{
			//Trying using a velocity change for the double jump so it has the same impact no matter what point in the jump you use it
			body.velocity = new Vector2 (body.velocity.x, airJumpVelocity);
			
			if (!grappleController.isGrappled)
			{
				currAirJumps--;
			}
			
			FireOnJumpEvent ();
		}
	}
	
	void FixedUpdate ()
	{
		float horzAxisKey = Input.GetAxisRaw ("HorizontalKey");
		float horzAxisJoy = Input.GetAxisRaw ("HorizontalJoystick");
		float horzAxis = (horzAxisJoy == 0.0f) ? horzAxisKey : horzAxisJoy;
		
		if (isGrounded)
		{
		
			body.AddForce (new Vector2 (horzAxis, 0.0f) * groundedMoveForce);
			float velocity = Mathf.Clamp (body.velocity.x, -maxGroundVelocity, maxGroundVelocity);
			body.velocity = new Vector2 (velocity, body.velocity.y);

			if (Mathf.Abs (body.velocity.x) > 0.0f)
			{
				animator.speed = Mathf.Abs (body.velocity.x / maxGroundVelocity);
			}
			else if (horzAxis == 0.0f)
			{
				animator.speed = 1;
			}
		}
		else
		{
			body.AddForce (new Vector2 (horzAxis, 0.0f) * aerialMoveForce);
			animator.speed = 1;
		}
		
		if (horzAxis != 0.0f)
		{
			transform.localScale = new Vector3 (Mathf.Sign (horzAxis) * rightFacingLocalScale, transform.localScale.y);
		}
		
		animator.SetBool ("isGrounded", isGrounded);
		animator.SetFloat ("velocity_y", body.velocity.y);
		animator.SetFloat ("velocity_x", Mathf.Abs (body.velocity.x));
	}
	
	private void UpdateGroundedStatus ()
	{
		float distance = col.bounds.extents.y + 0.2f;
		int layerMask = 1 << LayerMask.NameToLayer ("Ground");
		RaycastHit2D hit = Physics2D.BoxCast (col.bounds.center, new Vector3 (col.bounds.size.x - 0.1f, col.bounds.size.y - 0.1f), 0.0f, -transform.up, distance, layerMask);
		isGrounded = (hit.collider == null) ? false : true;
		
		if (isGrounded)
		{
			RegainAirJumps ();
		}
	}

	private void UpdateWallTouchStatus ()
	{
		float raycastDistance = col.bounds.extents.x + 0.2f;
		LayerMask raycastLayerMask = PhysicsLayers.WALLJUMP_LAYERMASK;

		RaycastHit2D topHit = Physics2D.Raycast (raycastPositionTop.position, transform.right, raycastDistance, raycastLayerMask);
		RaycastHit2D midHit = Physics2D.Raycast (raycastPositionMid.position, transform.right, raycastDistance, raycastLayerMask);
		RaycastHit2D botHit = Physics2D.Raycast (raycastPositionBot.position, transform.right, raycastDistance, raycastLayerMask);
		isTouchingWallRight = (topHit.collider != null || botHit.collider != null);


		topHit = Physics2D.Raycast (raycastPositionTop.position, -transform.right, raycastDistance, raycastLayerMask);
		midHit = Physics2D.Raycast (raycastPositionMid.position, -transform.right, raycastDistance, raycastLayerMask);
		botHit = Physics2D.Raycast (raycastPositionBot.position, -transform.right, raycastDistance, raycastLayerMask);
		isTouchingWallLeft = (topHit.collider != null || midHit.collider != null || botHit.collider != null);
	}
	
	private void FireOnJumpEvent ()
	{
		if (OnJump != null)
		{
			OnJump ();
		}
	}
	
	private void RegainAirJumps ()
	{
		currAirJumps = maxAirJumps;
	}
	
	void HandleOnGrappleAttached ()
	{
		RegainAirJumps ();
	}
}


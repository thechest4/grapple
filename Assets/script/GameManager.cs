using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class GameManager : MonoBehaviour
{
	private const string SCENE_NAME = "LevelGenTest";
	
	#region StartGameUI
	[SerializeField]
	private GameObject countdownPanel;
	[SerializeField]
	private Text countdownText;
	[SerializeField]
	private GameObject startPanel;
	#endregion
	
	#region EndGameUI
	[SerializeField]
	private GameObject endGameUIParent;
	[SerializeField]
	private Text finalScoreText;
	#endregion
	
	[SerializeField]
	private LifespanTimer lifespanTimer;
	
	private int countdownLength = 3;
	private bool isGameOver = false;
	private Coroutine currentCoroutine;

    public static float difficultyMultiplier = 0.5f;
    public static float timeSinceGameStart = 0f;
    public static event Action OnGameStarted;
	
	void Start ()
	{	
		currentCoroutine = StartCoroutine (StartGameCoroutine ());
		GarbageCollector.OnPlayerDeath += HandleOnPlayerDeath;
		
		endGameUIParent.SetActive (false);
		FallingPlatform.hasStarted = false;
	}

	void HandleOnPlayerDeath ()
	{
		if (currentCoroutine != null)
		{
			countdownPanel.SetActive (false);
			startPanel.SetActive (false);
		
			StopCoroutine (currentCoroutine);
		}
	
		lifespanTimer.StopTimer ();
		endGameUIParent.SetActive (true);
		finalScoreText.text = lifespanTimer.GetScore ();
		
		isGameOver = true;
	}
	
	void Update ()
	{
		if (isGameOver && Input.anyKeyDown)
		{
			Application.LoadLevel (SCENE_NAME);
		}

        timeSinceGameStart += Time.deltaTime;
	}
	
	private IEnumerator StartGameCoroutine ()
	{
		startPanel.SetActive (false);
		countdownPanel.SetActive (true);
		countdownText.text = "" + countdownLength;
        timeSinceGameStart = 0f;
		
		for (int i = countdownLength; i > 0; i--)
		{
			countdownText.text = "" + i;
			yield return new WaitForSeconds (1.0f);
		}
		
		
		countdownPanel.SetActive (false);
		startPanel.SetActive (true);
		FallingPlatform.hasStarted = true;
		
		if (OnGameStarted != null)
		{
			OnGameStarted ();
		}
		
		yield return new WaitForSeconds (0.5f);
		
		startPanel.SetActive (false);
	}
	
	void OnDestroy ()
	{
		GarbageCollector.OnPlayerDeath -= HandleOnPlayerDeath;
	}
}


﻿using UnityEngine;
using System.Collections;

public class FallingPlatform : MonoBehaviour
{	
	public static bool hasStarted = false;

	// Update is called once per frame
	void Update ()
    {
    	if (FallingPlatform.hasStarted)
		{
			Vector3 newPosition = transform.position;
			newPosition.y -= (Time.deltaTime * (GameManager.timeSinceGameStart / 60.0f + GameManager.difficultyMultiplier));
			transform.position = newPosition;
    	}
	}
}

using UnityEngine;
using System;
using System.Collections;

public class PlayerProjectileController : MonoBehaviour
{
	[SerializeField]
	private GrappleController grappleController;
	[SerializeField]
	private GameObject projectilePrefab;
	[SerializeField]
	private LineRenderer lineRenderer;
	
	private GameObject currentProjectile = null;
	
	void Start ()
	{
		grappleController.OnGrappleDetached += HandleOnGrappleDetached;
	}
	
	void OnDestroy ()
	{
		grappleController.OnGrappleDetached -= HandleOnGrappleDetached;
	}
	
	void Update ()
	{
		if (Input.GetButtonDown ("FireGrappleController") || Input.GetButtonDown ("FireGrappleMouse"))
		{
			if (currentProjectile == null)
			{
				SpawnProjectile (Input.GetButtonDown ("FireGrappleController"));
			}
			else
			{
				if (currentProjectile.GetComponent<GrappleProjectileController> ().ReelIn ())
				{
					currentProjectile = null;
				}
			}
		}
		
		if (lineRenderer.enabled)
		{
			if (currentProjectile != null)
			{
				lineRenderer.SetPosition (0, transform.position);
				lineRenderer.SetPosition (1, currentProjectile.GetComponent<GrappleProjectileController> ().jointConnectionPoint.position);
			}
			else
			{
				lineRenderer.enabled = false;
			}
		}
	}
	
	private void SpawnProjectile (bool useJoystickAngle)
	{
		currentProjectile = (GameObject)GameObject.Instantiate (projectilePrefab, transform.position, Quaternion.identity);
		Vector2 direction = Vector2.zero;

		if (useJoystickAngle)
		{
			direction = CalculateJoystickAngle (Input.GetAxisRaw ("VerticalJoystick"), Input.GetAxisRaw ("HorizontalJoystick"));
		}
		else
		{
			direction = Camera.main.ScreenToWorldPoint (Input.mousePosition) - transform.position;
		}


		currentProjectile.GetComponent<GrappleProjectileController> ().Initialize (direction.normalized, transform);
		currentProjectile.transform.right = direction.normalized;
		
		lineRenderer.enabled = true;
	}

	private Vector2 CalculateJoystickAngle (float vertAxis, float horzAxis)
	{
		float angleInDeg = Mathf.Atan2 (vertAxis, horzAxis) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis (angleInDeg, Vector3.forward);
		Vector3 direction = q * transform.right;

		if (horzAxis == 0.0f && vertAxis == 0.0f)
		{
			direction = transform.up;
		}

		return direction;
	}
	
	private void HandleOnGrappleDetached ()
	{
		//TODO: Have the grappling hook projectile reel back in to the player
		GameObject.Destroy (currentProjectile);
		currentProjectile = null;
		lineRenderer.enabled = false;
	}
}


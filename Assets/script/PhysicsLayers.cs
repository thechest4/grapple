﻿using UnityEngine;
using System.Collections;

public class PhysicsLayers
{
	public static int PLAYER_LAYER = LayerMask.NameToLayer ("Player");
	public static int TRIGGER_LAYER = LayerMask.NameToLayer ("Trigger");

	public static int WALLJUMP_LAYERMASK = ~((1 << PLAYER_LAYER) | (1 << TRIGGER_LAYER));
}


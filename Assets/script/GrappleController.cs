using UnityEngine;
using System;
using System.Collections;

public class GrappleController : MonoBehaviour
{
	//TODO: Figure out if there is a better way to handle communicating between scripts that doesn't involve each component having a reference to each other component
	[SerializeField]
	private PlayerController playerController;
	[SerializeField]
	private DistanceJoint2D distanceJoint;
	
	public bool isGrappled {get;set;}
	
	[SerializeField]
	private float grappleReelSpeed = 0.25f;
	
	private Transform grappleProjectileTransform;

	public event Action OnGrappleAttached;
	public event Action OnGrappleDetached;

	void Start ()
	{
		GrappleProjectileController.OnGrappleProjectileCollision += AttachJoint;
		playerController.OnJump += HandleOnJump;
		isGrappled = false;
	}
	
	void OnDestroy ()
	{
		GrappleProjectileController.OnGrappleProjectileCollision -= AttachJoint;
		playerController.OnJump -= HandleOnJump;
	}

	void Update ()
	{
		if (isGrappled)
		{
			float vertAxisKey = Input.GetAxisRaw ("VerticalKey");
			float vertAxisJoy = Input.GetAxisRaw ("VerticalJoystick");
			float vertAxis = (vertAxisJoy == 0.0f) ? vertAxisKey : vertAxisJoy;
			if (vertAxis != 0.0f)
			{
				distanceJoint.distance -= vertAxis * grappleReelSpeed * Time.deltaTime;
			}
			
			if (Input.GetButtonDown ("FireGrappleController") || Input.GetButtonDown ("FireGrappleMouse"))
			{
				DetachJoint ();
			}
			
			//Update the joint's anchor position in case the projectile has moved
			distanceJoint.connectedAnchor = grappleProjectileTransform.position;
		}
	}

	private void AttachJoint (Transform argProjectileTransform)
	{
		grappleProjectileTransform = argProjectileTransform;
	
		distanceJoint.enabled = true;
		distanceJoint.connectedAnchor = grappleProjectileTransform.position;
		distanceJoint.distance = Vector3.Distance (transform.position, grappleProjectileTransform.position);
		
		isGrappled = true;
		FireOnGrappleAttachedEvent ();
	}	
	
	private void DetachJoint ()
	{
		distanceJoint.enabled = false;
		isGrappled = false;
		FireOnGrappleDetachedEvent ();
	}
	
	private void FireOnGrappleAttachedEvent ()
	{
		if (OnGrappleAttached != null)
		{
			OnGrappleAttached ();
		}
	}
	
	private void FireOnGrappleDetachedEvent ()
	{
		if (OnGrappleDetached != null)
		{
			OnGrappleDetached ();
		}
	}
	
	private void HandleOnJump ()
	{
		if (isGrappled)
		{
			DetachJoint ();
		}
	}
}

